#include <stdio.h>
#define IN 1
#define OUT 0

int main(void) 
{
    int c;
    int pos = OUT;
    
    while ((c = getchar()) != EOF) {
        if (c == '\t' || c == '\n' || c == ' ') {
            if (pos == IN) {
                putchar('\n');
            }
            pos = OUT;
        } else {
            putchar(c);
            pos = IN;
        }
    }
    return 1;
}
