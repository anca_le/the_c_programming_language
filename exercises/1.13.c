//Write a program to print a histogram of the lengths of words in its input. 
//It is easy to draw the histogram with the bars horizontal; a vertical orientation is more challenging. 


#include <stdio.h>
#define IN 1
#define OUT 0

int main(void) 
{
    //get the counts
    int c;
    int hist[3];
    int pos = OUT;
    int count = 0;
    hist[0] = hist[1] = hist[2] = 0;
    c = '\n';
    while (c != EOF) {
        //need to account for files that end with a character other than a word delimiter
        c = getchar();
        if (c == '\t' || c == ' ' || c == '\n' || c == EOF) { 
            //outside word
            if (pos == IN) { 
                //just left the word
                if (count >= 0 && count < 5) hist[0]++;
                else if (count >= 5 && count <10) hist[1]++;
                else hist[2]++; 
            }
            pos = OUT;
        } else { 
            //inside word
            if (pos == OUT) {
                //start of a new word
                pos = IN;
                count = 0;
            }
            count++;
        }
    }
    
    //draw the histogram (horizontally)
    int index = 0;
    int i;
    while (index <= 2) {
        if (index == 0)         printf("\n[0..5)  : ");
        else if (index == 1)    printf("\n[5..10) : ");
        else                    printf("\n[10..)  : ");
        
        for (i = 0; i < hist[index]; i++) {
            putchar('-');
        }
        index++;
    }
    
    putchar('\n');
    
    return 1;
}
