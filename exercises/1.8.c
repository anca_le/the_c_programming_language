#include <stdio.h>

#define BLANK ' '
#define TAB '\t'
#define NEW_LINE '\n'

main() 
{
    int blank_no = 0, tab_no = 0, newline_no = 0;
    int c;
    while ((c = getchar()) != EOF) {
        if (c == BLANK) blank_no ++;
        else if (c == TAB) tab_no ++;
        else if (c == NEW_LINE) newline_no ++;
    }
    
    printf("No of blanks is %d\nNo of tabs is %d\nNo of new lines is %d\n", blank_no, tab_no, newline_no);
}
