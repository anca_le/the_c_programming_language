#include <stdio.h>

main() 
{
    int c;
    int special_character = 0;
    while ((c = getchar()) != EOF) {
    
        if (c == '\t') {
            putchar('\\'); 
            putchar('t'); 
            special_character++;
        }
        
        if (c == '\b') {
            putchar('\\'); 
            putchar('b'); 
            special_character++;
        }
        
        if (c == '\\') {
            putchar('\\'); 
            putchar('\\'); 
            special_character++;
        }
        
        if (special_character == 0) putchar(c);
        
        if (special_character > 0) special_character = 0;
    }
}
