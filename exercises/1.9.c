#include <stdio.h>

main()
{
    int c;
    int count = 0;
    
    while ((c = getchar()) != EOF) {
        if (c == ' ') count ++;
        if (c != ' ') count = 0;
        if (count <= 1) putchar(c);
    }
}
