#include <stdio.h>

#define n 8
#define MAXLENGTH 100
#define TAB '\t'
#define SPACE ' '

void detab(char s[]);
int getln(char s[], int lim);

int main()
{
	char s[MAXLENGTH];
    int len;
    while ((len = getln(s, MAXLENGTH)) > 0) {
        detab(s);
    }
}


void detab(char s[]) {
	int i;
	int tab_i = 0;
	for(i = 0; s[i] != '\0'; i++) {
		if (s[i] != TAB) {
			putchar(s[i]);
			tab_i++;
		} else {
			int spaces = n - (tab_i % n);
			int j;
			for (j = 0; j < spaces; j++) {
				putchar(SPACE);
			}
			tab_i = 0;
		}
	}
}

int getln(char s[], int lim) {
    int c, i;
    for (i = 0; i < lim -1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
    if (c == '\n') { 
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}


