#include <stdio.h>

#define MAXLENGTH 1000

int getln(char a[], int lim);
void reverse(char s[], int len, char r[]);
void printline(char s[], int len);

int main()
{
    char s[MAXLENGTH];
    int len;
    while ((len = getln(s, MAXLENGTH)) > 0) {
        char rev[len];
        reverse(s, len, rev);
        printline(rev, len); 
    }
}

int getln(char s[], int lim) {
    int c, i;
    for (i = 0; i < lim -1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
    if (c == '\n') { 
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

void printline(char s[], int len) {
    int i;
    for (i = 0; i < len; i++) {
        if (s[i] != '\n') {
            putchar(s[i]);
        }
    }
    putchar('\n');
}

void reverse(char s[], int len, char b[]) {
    int i;
    for (i = 0; i < len; i ++) {
        b[i] = s[len - i - 1];
    }
}

void reverse2(char s[]) {
    int len = 0;
    while (s[len] != '\0') {
        len++;
    }
    
    for (int i = 0; i < len; i++) {
        
    }
}

